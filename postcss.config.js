module.exports = {
  plugins: [require("tailwindcss"), require("autoprefixer")],
  variants: {
    extend: {
      // ...
      borderCollapse: ["hover", "focus"],
    },
  },
};
