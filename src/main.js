import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";
import router from "./routes";
import Vuesax from "vuesax";
import store from "./store";
import Vuex from "vuex";
import draggable from "vuedraggable";
import BootstrapVue from "bootstrap-vue";

import BaseCard from "./components/ui/BaseCard.vue";
import BaseButton from "./components/ui/BaseButton.vue";
import BaseSpinner from "./components/ui/BaseSpinner.vue";

import "bootstrap-vue/dist/bootstrap-vue-icons.min.css";
import "vuesax/dist/vuesax.css";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);

Vue.use(Vuesax);
Vue.use(VueRouter);
Vue.use(store);
Vue.use(Vuex);
Vue.use(BootstrapVue);
Vue.use(draggable);

Vue.config.productionTip = false;
Vue.component("base-card", BaseCard);
Vue.component("base-button", BaseButton);

Vue.component("base-spinner", BaseSpinner);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
