import Vue from "vue";
import Vuex from "vuex";

import BasicDetails from "./modules/BasicForm/index.js";
import AdvancedDetails from "./modules/AdvancedForm/index.js";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    BasicDetails,
    AdvancedDetails,
  },
});
