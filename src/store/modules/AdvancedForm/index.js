const advancedDetails = {
  state: {
    userData: [],
  },

  getters: {
    User_Data: (state) => state.userData,
  },

  actions: {
    Set_User: ({ commit }) => {
      commit("Set_Users", response.data);
    },
    Add_Data: ({ commit }, payload) => {
      console.log(payload);
      commit("Add_New_Data", payload);
    },
  },

  mutations: {
    Add_New_Data: (state, payload) => (state.userData = payload),
  },
};
export default advancedDetails;
