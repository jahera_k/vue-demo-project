const basicDetails = {
  state: {
    users: [],
  },

  getters: {
    User_List: (state) => state.users,
  },

  actions: {
    Add_Users: ({ commit }, payload) => {
      commit("Add_New_User", payload);
    },
  },

  mutations: {
    Add_New_User: (state, payload) => {
      state.users = payload;
    },
  },
};
export default basicDetails;
