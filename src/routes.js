import Vue from "vue";
import Router from "vue-router";
import BasicForm from "./components/BasicForm";
import AdvancedForm from "./components/AdvancedForm";
import AgGridTable from "./components/AgGridTable";
import BasicDetails from "./components/BasicDetails";
import AdvancedDetails from "./components/AdvancedDetails";
import VueDraggable from "./components/draggable/VueDraggable";
import ChatApp from "./components/ChatApp";
Vue.use(Router);
const router = new Router({
  mode: "history",

  routes: [
    { path: "/", component: BasicForm },
    { path: "/advanced", component: AdvancedForm },
    { path: "/agGrid", component: AgGridTable },
    { path: "/basicDetails", component: BasicDetails },
    { path: "/advancedDetails", component: AdvancedDetails },
    { path: "/draggable", component: VueDraggable },
    { path: "/chatapp", component: ChatApp },
  ],
});
export default router;
