import axios from "axios";

export const HTTP = axios.create({
  baseURL: `https://dummyapi.io/data/v1/`,
  headers: {
    "Content-type": "application/json",
    "APP-ID": "61a0c13713c22e27e3790c3d",
  },
});
